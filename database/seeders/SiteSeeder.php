<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\Lesson;
use Illuminate\Database\Seeder;
use Intervention\Image\ImageManagerStatic as Image;

class SiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content_ru = '<h2>Познакомьтесь С Ларавелем</h2>
        <p>Laravel - это фреймворк веб-приложения с выразительным, элегантным синтаксисом. Веб-фреймворк обеспечивает структуру и отправную точку для создания вашего приложения, позволяя вам сосредоточиться на создании чего - то удивительного, пока мы потеем над деталями.</p>
        <p>Laravel стремится обеспечить удивительный опыт разработчика, предоставляя при этом мощные функции, такие как тщательная инъекция зависимостей, выразительный уровень абстракции базы данных, очереди и запланированные задания, модульное и интеграционное тестирование и многое другое.</p>
        <p>Независимо от того, являетесь ли вы новичком в PHP или веб-фреймворках или имеете многолетний опыт работы, Laravel-это фреймворк, который может расти вместе с вами. Мы поможем вам сделать первые шаги в качестве веб-разработчика или дадим вам толчок, когда вы поднимете свой опыт на новый уровень. Мы не можем дождаться, чтобы увидеть, что вы строите.</p>
        <h3>Почему Именно Ларавель?</h3>
        <p>Существует множество инструментов и фреймворков, доступных вам при создании веб-приложения. Однако мы считаем, что Laravel-это лучший выбор для создания современных веб-приложений с полным стеком.</p>';

        $content_en = '<h2>Meet Laravel</h2>
        <p>Laravel is a web application framework with expressive, elegant syntax. A web framework provides a structure and starting point for creating your application, allowing you to focus on creating something amazing while we sweat the details.</p>
        <p>Laravel strives to provide an amazing developer experience, while providing powerful features such as thorough dependency injection, an expressive database abstraction layer, queues and scheduled jobs, unit and integration testing, and more.</p>
        <p>Whether you are new to PHP or web frameworks or have years of experience, Laravel is a framework that can grow with you. We\'ll help you take your first steps as a web developer or give you a boost as you take your expertise to the next level. We can\'t wait to see what you build.</p>
        <h3>Why Laravel?</h3>
        <p>There are a variety of tools and frameworks available to you when building a web application. However, we believe Laravel is the best choice for building modern, full-stack web applications.</p>
        <h4>A Progressive Framework</h4>
        <p>We like to call Laravel a "progressive" framework. By that, we mean that Laravel grows with you. If you\'re just taking your first steps into web development, Laravel\'s vast library of documentation, guides, and <a href="https://laracasts.com">video tutorials</a> will help you learn the ropes without becoming overwhelmed.</p>';

        $content_fr = '<h2>Répondre À Laravel</h2>
        <p>Laravel est un framework d\'applications web avec expressif, élégant syntaxe. Un framework web offre une structure et un point de départ pour la création de votre application, vous permettant de vous concentrer sur la création de quelque chose d\'étonnant alors que nous transpirons les détails.</p>
        <p>Laravel s\'efforce d\'offrir une incroyable expérience de développement, tout en offrant des fonctionnalités puissantes telles que le sérieux de l\'injection de dépendance, expressif couche d\'abstraction de base, les files d\'attente et des tâches planifiées, unitaires et les tests d\'intégration, et plus encore.</p>
        <p>Si vous êtes nouveau à PHP ou frameworks web ou ont des années d\'expérience, Laravel est un cadre qui peut grandir avec vous. Nous allons vous aider à faire vos premiers pas en tant que développeur web ou vous donner un coup de pouce que vous prenez votre expertise au niveau suivant. Nous ne pouvons pas attendre de voir ce que vous construisez.</p>
        <h3>Pourquoi Laravel?</h3>
        <p>Il existe une variété d\'outils et de cadres disponibles pour vous lors de la construction d\'une application web. Cependant, nous croyons que Laravel est le meilleur choix pour bâtiment moderne, full-stack applications web.</p>';

        for ($i=1; $i <= 7; $i++) { 
            $course = Course::create([
                'number' => $i,
                'name_ru' => 'Название курса ' . $i,
                'name_en' => 'Course name ' . $i,
                'name_fr' => 'Nom du cours ' . $i,
                'price_rub' => rand(10000, 30000),
                'price_usd' => rand(120, 360),
                'price_eur' => rand(90, 270),
            ]);
            $this->saveImage('Course name ' . $i, 'images/courses/' . $course->id . '.jpg');
            for ($ii=1; $ii <= 7; $ii++) { 
                $lesson = $course->lessons()->create([
                    'number' => $ii,
                    'name_ru' => 'Название урока ' . $ii,
                    'name_en' => 'Lesson name ' . $ii,
                    'name_fr' => 'Titre de la leçon ' . $ii,
                    'content_ru' => $content_ru,
                    'content_en' => $content_en,
                    'content_fr' => $content_fr,
                ]);
                $this->saveImage('Lesson name ' . $ii, 'images/lessons/' . $lesson->id . '.jpg');
            }
        }
    }

    public function saveImage(string $text, string $path)
    {
        $fcont = file_get_contents('http://placehold.it/800x600/d2691e/fff?text=' . $text);
        Image::make($fcont)->resize(
            config('app.upload_image.max_width', 800),
            config('app.upload_image.max_height', 600),
            function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            }
        )->save(
            public_path($path),
            config('app.upload_image.quality', 60)
        );
    }
}
