const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
        'resources/js/summernote.js',
        'resources/js/imageUploader.js',
        'resources/js/app.js',
    ], 'public/js/app.js')
    .scripts('resources/js/localization/ru.js', 'public/js/localization/ru.js')
    .scripts('resources/js/localization/en.js', 'public/js/localization/en.js')
    .scripts('resources/js/localization/fr.js', 'public/js/localization/fr.js')
    .styles('resources/css/*.css', 'public/css/app.css');
