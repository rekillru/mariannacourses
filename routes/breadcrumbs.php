<?php

// Main

use App\Models\Course;
use App\Models\Lesson;

Breadcrumbs::for('main', function ($trail) {
    $trail->push(__('Main'), route('main'));
});

Breadcrumbs::for('course', function ($trail, Course $course) {
    $trail->parent('main');
    $trail->push(__('Course') . ': ' . $course->name, route('course.show', $course));
});

Breadcrumbs::for('lesson', function ($trail, Course $course, Lesson $lesson) {
    $trail->parent('course', $course);
    $trail->push(__('Lesson') . ': ' . $lesson->name, route('lesson.show', $lesson));
});
