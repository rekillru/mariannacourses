<?php

use App\Http\Controllers\Admin\CourseController;
use App\Http\Controllers\Admin\HomeController as AdminHomeController;
use App\Http\Controllers\Admin\ImageController;
use App\Http\Controllers\Admin\InteractiveController;
use App\Http\Controllers\Admin\LessonController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\BuyController;
use App\Http\Controllers\CourseController as SiteCourseController;
use App\Http\Controllers\LessonController as SiteLessonController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InteractiveController as SiteInteractiveController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('main');
Route::get('set_lang/{lang}', [HomeController::class, 'setLang'])->name('set_lang');
Route::get('course/{course}', [SiteCourseController::class, 'show'])->name('course.show');
Route::get('buy/{course}', [BuyController::class, 'show'])->name('buy.show');
Route::get('admin', [AdminHomeController::class, 'index'])->name('admin');

Route::middleware(['verified'])->group(function () {
    // Роуты закрытые для неверифицированных
    Route::get('lesson/{lesson}', [SiteLessonController::class, 'show'])->name('lesson.show');
    Route::get('lesson/{lesson}/{user?}', [SiteLessonController::class, 'show'])->middleware('admin')->name('lesson.show');
    Route::post('interactive/{lesson}', [SiteInteractiveController::class, 'store'])->name('interactive.store');
    Route::post('interactive/{lesson}/{user?}', [SiteInteractiveController::class, 'store'])->middleware('admin')->name('interactive.store');
    Route::post('buy/{course}', [BuyController::class, 'store'])->name('buy.store');

    Route::middleware(['admin'])->prefix('admin')->group(function () {
        // Роуты открытые только для админа
        Route::get('/', [AdminHomeController::class, 'index'])->name('admin');
        Route::name('admin.')->group(function () {
            // Курсы
            Route::resource('course', CourseController::class);
            Route::resource('course/{course}/lesson', LessonController::class)->only(['create', 'store']);
            Route::get('course/{course}/restore', [CourseController::class, 'restore'])->name('course.restore');
            // Уроки
            Route::resource('lesson', LessonController::class)->only(['edit', 'update', 'destroy']);
            Route::get('lesson/{lesson}/restore', [LessonController::class, 'restore'])->name('lesson.restore');
            // Пользователи
            Route::resource('user', UserController::class)->only(['index', 'edit', 'update']);
            // Интерактив
            Route::get('interactive', [InteractiveController::class, 'index'])->name('interactive.index');
            Route::get('interactive/{lesson}', [InteractiveController::class, 'show'])->name('interactive.show');
            // Загрузчик изображений
            Route::post('image-uploader', [ImageController::class, 'upload'])->name('image-uploader');
        });
    });
});

require __DIR__.'/auth.php';
