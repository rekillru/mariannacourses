<?php

namespace App\Models;

use App\Models\Interactive;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

/**
 * Уроки
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property int $lesson_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property float $content_ru
 * @property float $content_en
 * @property float $content_fr
 */
class Lesson extends Model
{
    use HasFactory,
        SoftDeletes;

    protected $table = 'lessons';

    protected $fillable = [
        'number',
        'name_ru',
        'name_en',
        'name_fr',
        'content_ru',
        'content_en',
        'content_fr',
    ];

    protected $appends = [
        'name',
        'content',
        'poster',
        'question_count',
        'new_question_count',
        'has_question',
    ];

    public function getNameAttribute()
    {
        $localeName = 'name_' . app()->getLocale();

        return $this->$localeName;
    }

    public function getContentAttribute()
    {
        $localeContent = 'content_' . app()->getLocale();

        return $this->$localeContent;
    }

    public function getPosterAttribute()
    {
        $filePath = 'images/lessons/' . $this->id . '.jpg';

        if (!Storage::exists($filePath)) {
            return null;
        }

        return $filePath . '?' . time();
    }

    /**
     * Количество вопросов
     *
     * @return int
     */
    public function getQuestionCountAttribute(): int
    {
        return $this->interactives()->count();
    }

    public function getHasQuestionsAttribute()
    {
        return !!$this->new_question_count;
    }

    /**
     * Количество новых вопросов
     *
     * @return int
     */
    public function getNewQuestionCountAttribute(): int
    {
        return $this->interactives()
            ->with('messages', function ($builder) {
                $builder->latest();
            })
            ->get()
            ->filter(function ($interactive) {
                return !in_array($interactive->messages->first()->user_id, config('app.admin_user_ids'));
            })
            ->count();
    }

    /**
     * Курс
     *
     * @return BelongsTo
     */
    public function course(): BelongsTo
    {
        return $this->belongsTo(Course::class);
    }

    /**
     * Сообщения интерактива
     *
     * @return HasMany
     */
    public function interactives(): HasMany
    {
        return $this->hasMany(Interactive::class);
    }
}
