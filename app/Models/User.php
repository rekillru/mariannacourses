<?php

namespace App\Models;

use App\Models\Course;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'locale',
        'name',
        'login',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'is_admin',
    ];

    /**
     * Администратор или нет
     *
     * @return boolean
     */
    public function getIsAdminAttribute(): bool
    {
        return in_array($this->id, config('app.admin_user_ids', []));
    }

    /**
     * Выборка "Ученики"
     *
     * @param Builder $builder
     * @return void
     */
    public function scopeStudent(Builder $builder)
    {
        return $builder->whereNotIn('id', config('app.admin_user_ids', []));
    }

    /**
     * Курсы пользователя
     *
     * @return BelongsToMany
     */
    public function courses(): BelongsToMany
    {
        return $this->belongsToMany(Course::class);
    }

    /**
     * Сообщения интерактива
     *
     * @return HasMany
     */
    public function interactives(): HasMany
    {
        return $this->hasMany(Interactive::class, 'user_id');
    }
}
