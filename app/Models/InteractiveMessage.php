<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class InteractiveMessage extends Model
{
    use HasFactory;

    protected $table = 'interactive_messages';

    protected $fillable = [
        'content',
        'user_id',
    ];

    /**
     * Интерактив
     *
     * @return BelongsTo
     */
    public function interactive(): BelongsTo
    {
        return $this->belongsTo(Interactive::class, 'id', 'interactive_id');
    }
    
    /**
     * Автор
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
