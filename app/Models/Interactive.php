<?php

namespace App\Models;

use App\Models\Lesson;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Интерактив
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $user_id
 * @property int $lesson_id
 */
class Interactive extends Model
{
    use HasFactory;

    protected $table = 'interactives';

    protected $fillable = [
        'user_id',
    ];

    protected $with = [
        'user',
    ];

    protected $withCount = [
        'messages',
    ];

    protected $appends = [
        'last_message',
        'has_question',
    ];

    /**
     * Последнее сообщение
     *
     * @return InteractiveMessage
     */
    public function getLastMessageAttribute(): InteractiveMessage
    {
        return $this->messages()->latest()->first();
    }

    /**
     * Есть вопрос
     *
     * @return bool
     */
    public function getHasQuestionAttribute(): bool
    {
        return $this->user_id == $this->last_message->user_id;
    }

    /**
     * Автор
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Урок
     *
     * @return BelongsTo
     */
    public function lesson(): BelongsTo
    {
        return $this->belongsTo(Lesson::class);
    }

    /**
     * Сообщения интерактива
     *
     * @return HasMany
     */
    public function messages(): HasMany
    {
        return $this->hasMany(InteractiveMessage::class, 'interactive_id');
    }
}
