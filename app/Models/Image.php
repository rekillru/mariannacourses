<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Изображения
 *
 * @property int $id
 */
class Image extends Model
{
    use HasFactory;
}
