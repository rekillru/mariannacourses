<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

/**
 * Курсы
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_fr
 * @property float $price_rub
 * @property float $price_usd
 * @property float $price_eur
 */
class Course extends Model
{
    use HasFactory,
        SoftDeletes;

    protected $table = 'courses';

    protected $fillable = [
        'number',
        'name_ru',
        'name_en',
        'name_fr',
        'price_rub',
        'price_usd',
        'price_eur',
    ];

    protected $appends = [
        'name',
        'price',
        'poster',
    ];

    protected $withCount = [
        'lessons',
    ];

    public function getNameAttribute()
    {
        $localeName = 'name_' . app()->getLocale();

        return $this->$localeName;
    }

    public function getPriceAttribute()
    {
        $localePrice = 'price_' . config('app.currency.' . app()->getLocale());

        return $this->$localePrice;
    }

    public function getPosterAttribute()
    {
        $filePath = 'images/courses/' . $this->id . '.jpg';

        if (!Storage::exists($filePath)) {
            return null;
        }

        return $filePath . '?' . time();
    }

    /**
     * Уроки
     *
     * @return HasMany
     */
    public function lessons(): HasMany
    {
        return $this->hasMany(Lesson::class);
    }

    /**
     * Пользователи, оплатившие курс
     *
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
