<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Artesaos\SEOTools\Facades\SEOMeta;

class CourseController extends Controller
{
    public function show(Course $course)
    {
        SEOMeta::setTitle($course->name);
        SEOMeta::setDescription($course->name . ': ' . $course->price);
        
        $course->load(['lessons' => function ($builder) {
            $builder->orderBy('number', 'asc');
        }]);

        return view('course', [
            'course' => $course
        ]);
    }
}
