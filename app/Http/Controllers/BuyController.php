<?php

namespace App\Http\Controllers;

use App\Http\Requests\BuyRequest;
use App\Mail\BuyCourse;
use App\Models\Course;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Support\Facades\Mail;

class BuyController extends Controller
{
    public function show(Course $course)
    {
        SEOMeta::setTitle(__('Buy course') . ': ' . $course->name);

        if (!auth()->check()) {
            return redirect()->route('login')->withPageinfo(__('Authorize before buying'));
        }

        return view('buy', [
            'course' => $course,
        ]);
    }

    public function store(BuyRequest $request, Course $course)
    {
        Mail::to(config('app.admin_email'))->send(new BuyCourse($course));

        return redirect()->route('course.show', $course)
            ->withPageinfo(__('You request sent to teacher. Please wait.'));
    }
}
