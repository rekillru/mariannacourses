<?php

namespace App\Http\Controllers;

use App\Http\Requests\InteractiveRequest;
use App\Models\Lesson;
use App\Models\User;
use App\Services\InteractiveService;

class InteractiveController extends Controller
{
    public function store(
        InteractiveRequest $request,
        Lesson $lesson,
        ?User $user = null,
        InteractiveService $interactiveService
    )
    {
        $interactiveService->create($lesson, $request, $user);

        return redirect()->to(url()->previous() . '#dialog_form')
            ->withSuccess(auth()->user()->is_admin ? __('Ask sent') : __('Question sent'));
    }
}
