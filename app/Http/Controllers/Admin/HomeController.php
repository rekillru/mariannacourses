<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Interactive;
use App\Models\User;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        SEOMeta::setTitle(__('Dashboard'));

        $usersCount = User::student()->count();
        $questionsCount = Interactive::count();
        $newQuestionsCount = Interactive::with(['messages' => function ($b) {
            $b->latest();
        }])->get()->filter(function ($interactive) {
            return $interactive->messages->first()->user_id != config('app.admin_user_ids');
        })->count();
        $purchasedCoursesCount = DB::table('course_user')->count();

        return view('admin/home', [
            'usersCount' => $usersCount,
            'questionsCount' => $questionsCount,
            'newQuestionsCount' => $newQuestionsCount,
            'purchasedCoursesCount' => $purchasedCoursesCount,
        ]);
    }
}
