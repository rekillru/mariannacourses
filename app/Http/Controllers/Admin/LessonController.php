<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LessonRequest;
use App\Models\Course;
use App\Models\Lesson;
use App\Services\LessonService;
use Artesaos\SEOTools\Facades\SEOMeta;

class LessonController extends Controller
{
    public function create(Course $course)
    {
        SEOMeta::setTitle(__('Add lesson'));

        return view('admin/lesson/form', [
            'course' => $course
        ]);
    }

    public function store(Course $course, LessonRequest $request, LessonService $lessonService)
    {
        $lessonService->create($course, $request);

        return redirect()->route('admin.course.index')->withSuccess(__('Lesson created'));
    }

    public function edit($id)
    {
        SEOMeta::setTitle(__('Edit lesson'));

        $lesson = Lesson::withTrashed()->findOrFail($id);

        return view('admin/lesson/form', [
            'lesson' => $lesson,
        ]);
    }

    public function update(LessonRequest $request, $id, LessonService $lessonService)
    {
        $lesson = Lesson::withTrashed()->findOrFail($id);

        $lessonService->update($lesson, $request);

        return redirect()->route('admin.course.index')->withSuccess(__('Lesson updated'));
    }

    public function destroy($id, LessonService $lessonService)
    {
        $lesson = Lesson::withTrashed()->findOrFail($id);

        $lessonService->delete($lesson);

        session()->flash('success', __('Lesson deleted'));
    }

    public function restore($id, LessonService $lessonService)
    {
        $lesson = Lesson::withTrashed()->findOrFail($id);

        $lessonService->restore($lesson);

        return back()->withSuccess(__('Lesson restored'));
    }
}
