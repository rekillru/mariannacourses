<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CourseRequest;
use App\Models\Course;
use App\Services\CourseService;
use Artesaos\SEOTools\Facades\SEOMeta;

class CourseController extends Controller
{
    public function index()
    {
        SEOMeta::setTitle(__('Courses and lessons'));

        $courses = Course::withTrashed()->orderBy('number', 'asc')
            ->with(['lessons' => function ($builder) {
                $builder->withTrashed()->orderBy('number', 'asc');
            }])->paginate(config('app.per_page'));

        return view('admin/course/courses', [
            'courses' => $courses
        ]);
    }

    public function create()
    {
        SEOMeta::setTitle(__('Add course'));

        return view('admin.course/form');
    }

    public function store(CourseRequest $request, CourseService $courseService)
    {
        $courseService->create($request);

        return redirect()->route('admin.course.index')->withSuccess(__('Course created'));
    }

    public function edit($id)
    {
        SEOMeta::setTitle(__('Edit course'));

        $course = Course::withTrashed()->findOrFail($id);

        return view('admin.course/form', [
            'course' => $course,
        ]);
    }

    public function update(CourseRequest $request, $id, CourseService $courseService)
    {
        $course = Course::withTrashed()->findOrFail($id);

        $courseService->update($course, $request);

        return redirect()->route('admin.course.index')->withSuccess(__('Course successful updated'));
    }

    public function destroy($id, CourseService $courseService)
    {
        $course = Course::withTrashed()->findOrFail($id);

        $courseService->delete($course);

        session()->flash('success', __('Course deleted'));
    }

    public function restore($id, CourseService $courseService)
    {
        $course = Course::withTrashed()->findOrFail($id);

        $courseService->restore($course);

        return back()->withSuccess(__('Course restored'));
    }
}
