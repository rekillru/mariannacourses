<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Lesson;
use Artesaos\SEOTools\Facades\SEOMeta;

class InteractiveController extends Controller
{
    public function index()
    {
        SEOMeta::setTitle(__('Interactive'));

        $courses = Course::has('lessons.interactives')
            ->with('lessons', function ($b) {
                $b->has('interactives');
            })
            ->paginate(config('app.per_page', 10));

        return view('admin/interactive/interactives', [
            'courses' => $courses
        ]);
    }

    public function show(Lesson $lesson)
    {
        SEOMeta::setTitle(__('Interactive of lesson') . ': ' . $lesson->name);

        $interactives = $lesson->interactives;

        return view('admin/interactive/interactive', [
            'lesson' => $lesson,
            'interactives' => $interactives,
        ]);
    }
}
