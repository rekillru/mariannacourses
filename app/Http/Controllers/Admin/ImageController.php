<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ImageController extends Controller
{
    public function upload(Request $request, ImageService $imageService)
    {
        if ($file = $request->file('image')) {
            try {
                $imageModel = Image::create();
                $url = 'images/content/' . $imageModel->id . '.jpg';
                $imageService->saveImage($file, $url, !!$request->get('set_watermark'));
                DB::commit();
                return $url;
            } catch (\Exception $e) {
                DB::rollBack();
                return $e->getMessage();
            }
        }
    }
}
