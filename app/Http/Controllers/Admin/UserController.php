<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\User;
use App\Services\UserService;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        SEOMeta::setTitle(__('Students'));

        $users = User::student()->withCount('courses')->paginate(config('app.per_page'));

        return view('admin/user/users', [
            'users' => $users
        ]);
    }

    public function edit(User $user)
    {
        SEOMeta::setTitle(__('Purchased courses for') . ': ' . $user->name);

        $courses = Course::all();

        $user->load('courses');

        return view('admin/user/form', [
            'user' => $user,
            'courses' => $courses,
        ]);
    }

    public function update(User $user, Request $request, UserService $userService)
    {
        $userService->update($user, $request);

        return redirect()->route('admin.user.index')->withSuccess(__('Student access to courses updated'));
    }
}
