<?php

namespace App\Http\Controllers;

use App\Models\Lesson;
use App\Models\User;
use App\Services\LessonService;
use Artesaos\SEOTools\Facades\SEOMeta;

class LessonController extends Controller
{
    public function show(LessonService $lessonService, Lesson $lesson, ?User $user = null)
    {
        if (auth()->user()->cant('view', $lesson->course)) {
            return redirect()->route('buy.show', $lesson->course)->withPageinfo(__('To access the lesson, purchase the course'));
        }

        SEOMeta::setTitle($lesson->name . ' | ' . $lesson->course->name);
        SEOMeta::setDescription($lesson->name . '. ' . $lesson->course->name);

        $lessons = $lessonService->getOtherLessons($lesson);

        $interactive = $lessonService->getInteractive($lesson, $user ?? auth()->user());

        return view('lesson', [
            'lesson' => $lesson,
            'lessons' => $lessons,
            'interactive' => $interactive,
            'user' => $user,
        ]);
    }
}
