<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Artesaos\SEOTools\Facades\SEOMeta;

class HomeController extends Controller
{
    public function index()
    {
        SEOMeta::setTitle(__('Site name'));
        SEOMeta::setDescription(__('Site description'));

        $courses = Course::orderBy('number', 'asc')->paginate(config('app.per_page'));

        return view('home', [
            'courses' => $courses
        ]);
    }

    public function setLang(string $lang)
    {
        if (!in_array($lang, ['ru', 'en', 'fr'])) abort(404);

        if (auth()->check()) {
            auth()->user()->update(['locale' => $lang]);
        }
        session(['current_locale' => $lang]);

        return back();
    }
}
