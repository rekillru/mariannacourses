<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
    public function rules()
    {
        return [
            'number' => 'required|numeric',
            'name_ru' => 'required|max:150',
            'name_en' => 'required|max:150',
            'name_fr' => 'required|max:150',
            'price_rub' => 'required|numeric',
            'price_usd' => 'required|numeric',
            'price_eur' => 'required|numeric',
            'image' => 'nullable|image',
        ];
    }

    public function attributes()
    {
        return [
            'number' => __('Number'),
            'name_ru' => __('Title on russian'),
            'name_en' => __('Title on english'),
            'name_fr' => __('Title on french'),
            'price_rub' => __('Price in rub'),
            'price_usd' => __('Price in usd'),
            'price_eur' => __('Price in eur'),
            'image' => __('Poster'),
        ];
    }
}
