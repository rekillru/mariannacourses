<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BuyRequest extends FormRequest
{
    public function rules()
    {
        return [
            'age' => 'required|numeric',
            'reason' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'age' => __('Age'),
            'reason' => __('Reason'),
        ];
    }
}
