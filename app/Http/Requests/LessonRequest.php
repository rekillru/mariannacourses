<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LessonRequest extends FormRequest
{
    public function rules()
    {
        return [
            'number' => 'required|numeric',
            'name_ru' => 'required|max:150',
            'name_en' => 'required|max:150',
            'name_fr' => 'required|max:150',
            'content_ru' => 'required',
            'content_en' => 'required',
            'content_fr' => 'required',
            'image' => 'nullable|image',
        ];
    }

    public function attributes()
    {
        return [
            'number' => __('Number'),
            'name_ru' => __('Title on russian'),
            'name_en' => __('Title on english'),
            'name_fr' => __('Title on french'),
            'content_ru' => __('Content on russian'),
            'content_en' => __('Content on english'),
            'content_fr' => __('Content on french'),
            'image' => __('Poster'),
        ];
    }
}
