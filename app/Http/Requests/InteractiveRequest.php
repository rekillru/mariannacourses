<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InteractiveRequest extends FormRequest
{
    public function rules()
    {
        return [
            'content' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'content' => auth()->user()->is_admin ? __('Your ask') : __('Your question'),
        ];
    }
}
