<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check()) {
            $locale = auth()->user()->locale ?? config('app.locale');
        } else {
            $locale = session('current_locale', config('app.locale'));
        }
        
        app()->setLocale($locale);

        return $next($request);
    }
}
