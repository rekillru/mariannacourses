<?php

namespace App\Mail;

use App\Models\Interactive;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InteractiveAnswered extends Mailable
{
    use SerializesModels;

    protected $interactive;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Interactive $interactive)
    {
        $this->interactive = $interactive;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Ответ на вопрос: ' . __('Site name'))
            ->from(config('app.sendback_email'))
            ->view('mails/interactive_answered', [
                'interactive' => $this->interactive,
            ]);
    }
}
