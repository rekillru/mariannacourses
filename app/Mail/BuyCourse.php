<?php

namespace App\Mail;

use App\Models\Course;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BuyCourse extends Mailable
{
    use SerializesModels;

    protected $course;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Course $course)
    {
        $this->course = $course;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('app.sendback_email'))
            ->subject('Покупка курса')
            ->view('mails/buy', [
                'course' => $this->course,
            ]);
    }
}
