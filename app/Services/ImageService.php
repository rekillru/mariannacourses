<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ImageService
{
    /**
     * Undocumented function
     *
     * @param UploadedFile $file
     * @param string $path
     * @return void
     */
    public function saveImage(UploadedFile $file, string $path, bool $setWatermark = false)
    {
        $folder = dirname($path);

        Storage::makeDirectory($folder);

        $imageIntervention = Image::make($file)->resize(
            config('app.upload_image.max_width', 800),
            config('app.upload_image.max_height', 600),
            function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            }
        );
        if ($setWatermark) {
            $watermark = Image::make(config('app.watermark.file_path'))->opacity(config('app.watermark.opacity'));
            $ratio = $imageIntervention->height() / $imageIntervention->width();
            $watermarkSize = config('app.watermark.size_in_percent') / 100;
            if ($ratio > 1) {
                $watermark->heighten($imageIntervention->height() * $watermarkSize);
                $watermark->widen(min($watermark->width(), $imageIntervention->width()));
            } else {
                $watermark->widen($imageIntervention->width() * $watermarkSize);
                $watermark->heighten(min($watermark->height(), $imageIntervention->height()));
            }
            $imageIntervention->insert(
                $watermark,
                config('app.watermark.placement'),
                round($imageIntervention->width() * (config('app.watermark.x_offset') / 100)),
                round($imageIntervention->height() * (config('app.watermark.y_offset') / 100))
            );
        }
        $imageIntervention->save(
            $path,
            config('app.upload_image.quality', 60)
        );
    }
}
