<?php

namespace App\Services;

use App\Models\Course;
use App\Models\Interactive;
use App\Models\Lesson;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LessonService
{
    protected $imageService;

    public function __construct(ImageService $imageService) {
        $this->imageService = $imageService;
    }

    public function create(Course $course, Request $request)
    {
        $lesson = $course->lessons()->create($request->all());

        if ($file = $request->file('image')) {
            $this->imageService->saveImage(
                $file,
                'images/lessons/' . $lesson->id . '.jpg',
                !!$request->get('set_watermark')
            );
        }

        return $lesson;
    }

    public function update(Lesson $lesson, Request $request)
    {
        $lesson->update($request->all());

        if ($file = $request->file('image')) {
            $this->imageService->saveImage(
                $file,
                'images/lessons/' . $lesson->id . '.jpg',
                !!$request->get('set_watermark')
            );
        }
    }

    public function delete(Lesson $lesson)
    {
        if ($lesson->trashed()) {
            $this->forceDelete($lesson);
        } else {
            $lesson->delete();
        }
    }

    public function forceDelete(Lesson $lesson)
    {
        $posterPath = 'images/lessons/' . $lesson->id . '.jpg';
        if (Storage::exists($posterPath)) {
            Storage::delete($posterPath);
        }
        $lesson->forceDelete();
    }

    public function restore(Lesson $lesson)
    {
        $lesson->restore();
    }

    public function getOtherLessons(Lesson $lesson)
    {
        return $lesson->course
            ->lessons()
            ->orderBy('number', 'asc')
            ->where('id', '!=', $lesson->id)
            ->get();
    }

    /**
     * Интерактив для пользователя
     *
     * @param Lesson $lesson
     * @param User $user
     * @return Interactive|null
     */
    public function getInteractive(Lesson $lesson, User $user): ?Interactive
    {
        return $lesson->interactives()->where('user_id', $user->id ?? auth()->id())->first();
    }
}
