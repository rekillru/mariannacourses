<?php

namespace App\Services;

use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CourseService
{
    protected $lessonService;
    protected $imageService;

    public function __construct(LessonService $lessonService, ImageService $imageService) {
        $this->lessonService = $lessonService;
        $this->imageService = $imageService;
    }

    public function create(Request $request)
    {
        $course = Course::create($request->all());

        if ($file = $request->file('image')) {
            $this->imageService->saveImage(
                $file,
                'images/courses/' . $course->id . '.jpg',
                !!$request->get('set_watermark')
            );
        }

        return $course;
    }

    public function update(Course $course, Request $request)
    {
        $course->update($request->all());

        if ($file = $request->file('image')) {
            $this->imageService->saveImage(
                $file,
                'images/courses/' . $course->id . '.jpg',
                !!$request->get('set_watermark')
            );
        }
    }

    public function delete(Course $course)
    {
        if ($course->trashed()) {
            $course->lessons->each(function ($lesson) {
                $this->lessonService->forceDelete($lesson);
            });
            $posterPath = 'images/courses/' . $course->id . '.jpg';
            if (Storage::exists($posterPath)) {
                Storage::delete($posterPath);
            }
            $course->users()->detach();
            $course->forceDelete();
        } else {
            $course->lessons->each(function ($lesson) {
                $this->lessonService->delete($lesson);
            });
            $course->delete();
        }
    }

    public function restore(Course $course)
    {
        $course->restore();
    }
}
