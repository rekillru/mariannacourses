<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Http\Request;

class UserService
{
    public function update(User $user, Request $request)
    {
        $user->courses()->sync($request->get('courses'));
    }
}
