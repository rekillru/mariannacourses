<?php

namespace App\Services;

use App\Mail\InteractiveAnswered;
use App\Models\Lesson;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class InteractiveService
{
    /**
     * Создание сообщения интерактива
     *
     * @param User $user
     * @param Lesson $lesson
     * @param Request $request
     * @param User|null $forUser
     * @return void
     */
    public function create(Lesson $lesson, Request $request, ?User $forUser = null)
    {
        $interactive = $lesson->interactives()->firstOrCreate([
            'user_id' => auth()->user()->is_admin ? $forUser->id : auth()->id(),
        ]);

        $interactive->messages()->create([
            'user_id' => auth()->id(),
            'content' => $request->get('content'),
        ]);
        
        /**
         * Отправка ученику уведомления об ответе
         */
        if (auth()->user()->is_admin) {
            app()->setLocale($interactive->user->locale);
            Mail::to($forUser)->send(new InteractiveAnswered($interactive));
            app()->setLocale(auth()->user()->locale);
        }
    }
}
