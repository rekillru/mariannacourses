<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-3">
                <div class="h5">{{ $interactive->user->name }}</div>
                <small class="help-block">{{ $interactive->created_at }}</small>
            </div>
            <div class="col-sm-9">
                {!! $interactive->content !!}
            </div>
        </div>
    </div>
</div>
