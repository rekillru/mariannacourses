<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <base href="/">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! SEOMeta::generate() !!}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.18/summernote-bs4.min.css" integrity="sha512-pDpLmYKym2pnF0DNYDKxRnOk1wkM9fISpSOjt8kWFKQeDmBTjSnBZhTd41tXwh8+bRMoSaFsRnznZUiH9i3pxA==" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/app.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="js/localization/{{ app()->getLocale() }}.js"></script>
    <script src="js/app.js?2"></script>

</head>
<body>

    <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">@lang('Site name')</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">@lang('Language'): @lang(app()->getLocale()) <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('set_lang', 'ru') }}">@lang('ru')</a></li>
                        <li><a href="{{ route('set_lang', 'en') }}">@lang('en')</a></li>
                        <li><a href="{{ route('set_lang', 'fr') }}">@lang('fr')</a></li>
                    </ul>
                </li>
                @guest
                    <li><a href="{{ route('login') }}">@lang('Sign in')</a></li>
                    <li><a href="{{ route('register') }}">@lang('Sign up')</a></li>
                @else
                    @if (auth()->user()->is_admin)
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ auth()->user()->name }} <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('admin') }}">@lang('Admin panel')</a></li>
                            </ul>
                        </li>
                    @else
                        <li>
                            <p class="navbar-text">{{ auth()->user()->name }}</p>
                        </li>
                    @endif
                    <li>
                        <a href="#" onclick="event.preventDefault();
                        document.getElementById('logout_form').submit();">@lang('Logout')</a>
                        <form method="POST" action="{{ route('logout') }}" id="logout_form">
                            @csrf
                        </form>
                    </li>
                @endguest
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>

    <div class="container">

        @if (auth()->check() && !(bool) auth()->user()->email_verified_at)
            <div class="alert alert-warning">
                @lang('Confirm your email to open all the functions of the site')
            </div>
        @endif

        @if ($pageinfo = session('pageinfo'))
            <div class="alert alert-info">{{ $pageinfo }}</div>
        @endif

        @yield('page')
        <br>
        <br>
    </div>
</body>
</html>
