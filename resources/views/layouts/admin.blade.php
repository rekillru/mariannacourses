@extends('layouts/app')

@section('page')
    <div class="row">
        <div class="col-md-3">
            
            <div class="btn-group-vertical btn-block">
                <a class="btn btn-default" href="{{ route('admin.course.index') }}" role="button">
                    @lang('Courses and lessons')
                </a>
                <a class="btn btn-default" href="{{ route('admin.user.index') }}" role="button">@lang('Students')</a>
                <a class="btn btn-default" href="{{ route('admin.interactive.index') }}" role="button">@lang('Interactive')</a>
            </div>
            
        </div>
        <div class="col-md-9">
            @yield('content')
        </div>
    </div>
@endsection
