<h1>@lang("Teacher's answer")</h1>
<p>@lang('Teacher answered on your question in lesson'): <a href="{{ route('lesson.show', $interactive->lesson) }}" target="_blank">
    {{ $interactive->lesson->name }}
</a></p>
