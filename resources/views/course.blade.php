@extends('layouts/app')

@section('page')
<div class="row">
    <div class="col-md-9">
        {{ Breadcrumbs::render('course', $course) }}
        <h1>@lang('Course'): {{ $course->name }}</h1>
        @foreach ($course->lessons as $lesson)
            <div class="row @if (!$loop->last) border-bottom @endif pt-10 pb-10">
                <div class="col-sm-3 col-md-2">
                    @if ($lesson->poster)
                        <img src="{{ $lesson->poster }}" alt="" class="img-responsive">
                    @endif
                </div>
                <div class="col-sm-6 col-md-8">
                    <h4>
                        {{ $lesson->number }}. 
                        @can('view', $course)
                        <a href="{{ route('lesson.show', $lesson) }}">
                            {{ $lesson->name }}
                        </a>
                        @else
                            {{ $lesson->name }}
                        @endcan
                    </h4>
                </div>
                <div class="col-sm-3 col-md-2">
                    @can('view', $course)
                        <a class="btn btn-default" href="{{ route('lesson.show', $lesson) }}">
                            @lang('More')
                        </a>
                    @endcan
                </div>
            </div>
        @endforeach
        
    </div>
    <div class="col-md-3">
        @if ($course->poster)
            <p>
                <img src="{{ $course->poster }}" alt="" class="img-responsive">
            </p>
            <br>
        @endif
        @cannot('view', $course)
            <div class="alert alert-warning">
                <h3 class="text-center">{{ $course->price }} @lang(config('app.currency.' . app()->getLocale()))</h3>
                <p>@lang('Buy a course to access the lessons')</p>
                <p class="text-center">
                    <a href="{{ route('buy.show', $course) }}" class="btn btn-warning btn-lg">
                        @lang('Buy')
                    </a>
                </p>
            </div>
        @endcannot
    </div>
</div>
    
@endsection
