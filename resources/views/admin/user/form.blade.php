@extends('layouts/admin')

@section('content')
    <h1>@lang('Purchased courses for'): {{ $user->name }}</h1>
    
    <form action="{{ route('admin.user.update', $user) }}" method="POST">
        @csrf
        @method('PATCH')
        
        @foreach ($courses as $course)
            <div class="checkbox">
                <label>
                    <input name="courses[]" @if ($user->courses->contains($course->id)) checked @endif type="checkbox" value="{{ $course->id }}">
                    {{ $course->name }}
                </label>
            </div>
        @endforeach
        
        <button type="submit" class="btn btn-primary">@lang('Save')</button>
    </form>
    
@endsection
