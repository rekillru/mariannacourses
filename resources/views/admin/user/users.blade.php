@extends('layouts/admin')

@section('content')
    <h1>@lang('Students')</h1>

    @include('alert')

    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>@lang('Name')</th>
                    <th>@lang('Courses count')</th>
                    <th>@lang('Control')</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
        
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->courses_count }}</td>
                        <td>
                            <a href="{{ route('admin.user.edit', $user) }}" class="btn btn-default btn-sm">
                                @lang('More')
                            </a>
                        </td>
                    </tr>

                @endforeach
            </tbody>
        </table>
    </div>
    
    {{ $users->links() }}
    
        
@endsection
