@extends('layouts/admin')

@section('content')
    <h1>@lang('Dashboard')</h1>
    <dl class="dl-horizontal">
        <dt style="white-space: normal">@lang('Students'):</dt>
        <dd>{{ $usersCount }}</dd>
    </dl>
    <dl class="dl-horizontal">
        <dt style="white-space: normal">@lang('Student questions'):</dt>
        <dd>{{ $questionsCount }}</dd>
    </dl>
    <dl class="dl-horizontal">
        <dt style="white-space: normal">@lang('New student questions'):</dt>
        <dd>{{ $newQuestionsCount }}</dd>
    </dl>
    <dl class="dl-horizontal">
        <dt style="white-space: normal">@lang('Purchased courses count'):</dt>
        <dd>{{ $purchasedCoursesCount }}</dd>
    </dl>
@endsection
