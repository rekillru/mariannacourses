@extends('layouts/admin')

@section('content')
    <h1>@lang('Interactive')</h1>  

    @foreach ($courses as $course)
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">@lang('Course'): {{ $course->name }}</h3>
            </div>  
            <div class="panel-body"> 
                <h5>@lang('Lessons'):</h5>
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>@lang('Title')</th>
                                <th>@lang('Student questions')</th>
                                <th>@lang('Questions without ask')</th>
                                <th>@lang('Control')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($course->lessons as $lesson)
                                <tr @if ($lesson->has_questions) class="success" @endif>
                                    <td>{{ $lesson->name }}</td>
                                    <td>{{ $lesson->question_count }}</td>
                                    <td>{{ $lesson->new_question_count }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-default btn-sm" href="{{ route('admin.interactive.show', $lesson) }}" role="button">@lang('More')</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div> 
                {{ $courses->links() }}
            </div>

        </div>
    @endforeach
        
@endsection
