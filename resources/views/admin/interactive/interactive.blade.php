@extends('layouts/admin')

@section('content')
    <h1>@lang('Interactive of lesson'): {{ $lesson->name }}</h1>  

    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>@lang('Name')</th>
                <th>@lang('Date of last message')</th>
                <th>@lang('Messages count')</th>
                <th>@lang('Control')</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($interactives as $interactive)
                <tr @if ($interactive->has_question) class="success" @endif>
                    <td>{{ $interactive->user->name }}</td>
                    <td>{{ $interactive->last_message->created_at }}</td>
                    <td>{{ $interactive->messages_count }}</td>
                    <td>
                        
                        <div class="btn-group">
                            <a class="btn btn-default" href="{{ route('lesson.show', [$lesson, $interactive->user]) }}#dialog_form" role="button">
                                @lang('More')
                            </a>
                        </div>
                        
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection
