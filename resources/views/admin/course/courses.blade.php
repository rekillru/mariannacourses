@extends('layouts/admin')

@section('content')
    <h1>@lang('Courses and lessons')</h1>

    <div class="btn-group form-group">
        <a class="btn btn-default" href="{{ route('admin.course.create') }}" role="button">
            @lang('Add course')
        </a>
    </div>

    @include('alert')

    @foreach ($courses as $course)
        
        <div class="panel panel-{{ $course->trashed() ? 'danger' : 'default' }}">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-9">
                        <h3 class="panel-title">{{ $course->number }}. {{ $course->name }}</h3>
                        <p class="help-block">@lang('Price'): {{ $course->price_rub }} rub / {{ $course->price_usd }} usd / {{ $course->price_eur }} eur</p>
                        <div class="btn-group">
                            <a class="btn btn-default" href="{{ route('admin.course.edit', $course) }}" role="button">@lang('Edit course')</a>
                            @if ($course->trashed())
                                <a class="btn btn-default" href="{{ route('admin.course.restore', $course) }}" role="button">@lang('Restore course')</a>
                            @else
                                <a class="btn btn-default" href="{{ route('admin.lesson.create', $course) }}" role="button">@lang('Add lesson')</a>
                            @endif
                            <a class="btn btn-danger" onclick="confirmDelete('{{ route('admin.course.destroy', $course) }}')" href="#" role="button">@lang('Delete course')</a>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        @if ($course->poster)
                            <img src="{{ $course->poster }}" alt="" class="img-responsive hidden-xs">
                        @endif
                    </div>
                </div>
            </div>

            @if ($course->lessons->isNotEmpty())
                  
                <div class="panel-body"> 
                    <h5>@lang('Lessons'):</h5>
                    <div class="table-responsive">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>@lang('Poster')</th>
                                    <th>@lang('Title')</th>
                                    <th>@lang('Control')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($course->lessons as $lesson)
                                    <tr @if ($lesson->trashed()) class="danger" @endif>
                                        <td style="max-width: 120px;">
                                            @if ($lesson->poster)
                                                <img src="{{ $lesson->poster }}" alt="" class="img-responsive">
                                            @endif
                                        </td>
                                        <td>{{ $lesson->number }}. {{ $lesson->name }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-default btn-sm" href="{{ route('admin.lesson.edit', $lesson) }}" role="button">@lang('Edit lesson')</a>
                                                @if ($lesson->trashed())
                                                    <a class="btn btn-default btn-sm" href="{{ route('admin.lesson.restore', $lesson) }}" role="button">@lang('Restore lesson')</a>
                                                @endif
                                                <a class="btn btn-danger btn-sm" onclick="confirmDelete('{{ route('admin.lesson.destroy', $lesson) }}')" href="#" role="button">@lang('Delete lesson')</a>
                                            </div>
                                            
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div> 
                    {{ $courses->links() }}
                </div>

            @endif

        </div>
    @endforeach
        
@endsection
