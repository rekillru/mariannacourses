@extends('layouts/admin')

@section('content')
    <h1>@lang((isset($lesson) ? 'Edit' : 'Add') . ' lesson')</h1>

    @error('*')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror

    <form action="{{ isset($lesson) ? route('admin.lesson.update', $lesson) : route('admin.lesson.store', [$course]) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @if (isset($lesson))
            @method('PATCH')
        @endif

        <div role="tabpanel clearfix">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-justified" role="tablist">
                <li role="presentation" class="active">
                    <a href="#ru" aria-controls="ru" role="tab" data-toggle="tab">@lang('ru')</a>
                </li>
                <li role="presentation">
                    <a href="#en" aria-controls="en" role="tab" data-toggle="tab">@lang('en')</a>
                </li>
                <li role="presentation">
                    <a href="#fr" aria-controls="fr" role="tab" data-toggle="tab">@lang('fr')</a>
                </li>
            </ul>
            <br>
            <div class="form-group">
                <label for="">@lang('Number')</label>
                <p class="help-block">@lang('This field is used to sort')</p>
                <input type="text" name="number" class="form-control" value="{{ old('number', $lesson->number ?? '') }}">
            </div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="ru">
                    <div class="form-group">
                        <label for="name_ru">@lang('Title on russian')</label>
                        <input type="text" name="name_ru" class="form-control" value="{{ old('name_ru', $lesson->name_ru ?? '') }}">
                    </div>
                    <div class="form-group">
                        <label for="content_ru">@lang('Content on russian')</label>
                        <textarea name="content_ru" class="summernote form-control">{{ old('content_ru', $lesson->content_ru ?? '') }}</textarea>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="en">
                    <div class="form-group">
                        <label for="name_en">@lang('Title on english')</label>
                        <input type="text" name="name_en" class="form-control" value="{{ old('name_en', $lesson->name_en ?? '') }}">
                    </div>
                    <div class="form-group">
                        <label for="content_en">@lang('Content on english')</label>
                        <textarea name="content_en" class="summernote form-control">{{ old('content_en', $lesson->content_en ?? '') }}</textarea>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="fr">
                    <div class="form-group">
                        <label for="name_fr">@lang('Title on french')</label>
                        <input type="text" name="name_fr" class="form-control" value="{{ old('name_fr', $lesson->name_fr ?? '') }}">
                    </div>
                    <div class="form-group">
                        <label for="content_fr">@lang('Content on french')</label>
                        <textarea name="content_fr" class="summernote form-control">{{ old('content_fr', $lesson->content_fr ?? '') }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">@lang('Poster')</label>
            <span class="help-block">@lang('Left empty if you not want to change')</span>
            <input type="file" name="image" accept="image/*">
        </div>
        <div class="form-group">
            <input type="hidden" name="set_watermark" value="0">
            <label for="set_watermark">
                <input type="checkbox" checked name="set_watermark" id="set_watermark" value="1"> @lang('Set watermark on poster')
            </label>
        </div>
        <button type="submit" class="btn btn-primary">@lang('Save')</button>
    </form>

@endsection
