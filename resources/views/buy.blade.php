@extends('layouts/app')

@section('page')
    <h1>@lang('Buy course'): {{ $course->name }}</h1>
    @include('error')
    <div class="row">
        <div class="col-md-6">
            <form action="{{ route('buy.store', $course) }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="">@lang('Age')</label>
                    <input type="text" name="age" required autofocus class="form-control" value="{{ old('age') }}">
                </div>
                <div class="form-group">
                    <label for="">@lang('Reason')</label>
                    <textarea cols="4" rows="4" name="reason" required class="form-control" style="resize: none">{{ old('reason') }}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">@lang('Send')</button>
            </form>
        </div>
    </div>
    
@endsection
