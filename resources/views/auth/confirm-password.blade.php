@extends('layouts/app')

@section('page')
    <h1>@lang('Confirm Password')</h1>
    <div class="help-block">
        @lang('This is a secure area of the application. Please confirm your password before continuing.')
    </div>
    @error('*')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <form method="POST" action="{{ route('password.confirm') }}">
        @csrf

        <input type="hidden" name="token" value="{{ $request->route('token') }}">

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="">@lang('Password')</label>
                    <input class="form-control" type="password" name="password" required autocomplete="current-password" />
                </div>

                <button type="submit" class="btn btn-primary">@lang('Confirm')</button>
            </div>
        </div>
        
    </form>
@endsection
