@extends('layouts/app')

@section('page')
    <h1>@lang('Verify email')</h1>
    <div class="help-block">
        @lang("Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn't receive the email, we will gladly send you another.")
    </div>
    @if (session('status') == 'verification-link-sent')
        <div class="alert alert-success">
            @lang('A new verification link has been sent to the email address you provided during registration.')
        </div>
    @endif
    @error('*')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <form method="POST" action="{{ route('verification.send') }}" class="text-left">
        @csrf
        <div class="row">
            <div class="col-sm-4">
                <button type="submit" class="btn btn-primary">@lang('Resend Verification Email')</button>
            </div>
        </div>
    </form>

@endsection
