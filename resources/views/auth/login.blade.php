@extends('layouts/app')

@section('page')
    <h1>@lang('Sign in')</h1>
    @include('error')
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="row">
            <div class="col-sm-4">
                <!-- Email Address -->
                <div class="form-group">
                    <label for="">@lang('Email')</label>
                    <input class="form-control" type="email" name="email" required autofocus value="{{ old('email') }}" />
                </div>
                <div class="form-group">
                    <label for="">@lang('Password')</label>
                    <input class="form-control" type="password" name="password" required autocomplete="current-password" />
                </div>
                <input type="hidden" name="remember" >
                
                @if (Route::has('password.request'))
                    <div class="form-group">
                        <a href="{{ route('password.request') }}">
                            @lang('Forgot your password?')
                        </a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('register') }}">@lang('Still not have account?')</a>      
                    </div>
                @endif 

                <button type="submit" class="btn btn-primary">@lang('Sign in')</button>
            </div>
        </div>
        
    </form>
@endsection
