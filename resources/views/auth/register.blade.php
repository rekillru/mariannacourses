@extends('layouts/app')

@section('page')
    <h1>@lang('Sign up')</h1>
    @error('*')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <form method="POST" action="{{ route('register') }}">
        @csrf

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="">@lang('Name')</label>
                    <input class="form-control" type="text" name="name" required autofocus value="{{ old('name') }}" />
                </div>
                <div class="form-group">
                    <label for="">@lang('Email')</label>
                    <input class="form-control" type="email" name="email" required value="{{ old('email') }}" />
                </div>
                <div class="form-group">
                    <label for="">@lang('Password')</label>
                    <input class="form-control" type="password" name="password" required autocomplete="current-password" />
                </div>
                <div class="form-group">
                    <label for="">@lang('Confirm Password')</label>
                    <input class="form-control" type="password" name="password_confirmation" required autocomplete="current-password" />
                </div>
                
                <div class="form-group">
                    <a href="{{ route('login') }}">
                        @lang('Already registered?')
                    </a>        
                </div>

                <button type="submit" class="btn btn-primary">@lang('Sign up')</button>
            </div>
        </div>
        
    </form>
@endsection
