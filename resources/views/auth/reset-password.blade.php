@extends('layouts/app')

@section('page')
    <h1>@lang('Reset password')</h1>
    @error('*')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <form method="POST" action="{{ route('password.update') }}">
        @csrf

        <input type="hidden" name="token" value="{{ $request->route('token') }}">

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="">@lang('Email')</label>
                    <input class="form-control" type="email" name="email" required value="{{ old('email', $request->email) }}" />
                </div>
                <div class="form-group">
                    <label for="">@lang('Password')</label>
                    <input class="form-control" type="password" name="password" required autocomplete="current-password" />
                </div>
                <div class="form-group">
                    <label for="">@lang('Confirm Password')</label>
                    <input class="form-control" type="password" name="password_confirmation" required autocomplete="current-password" />
                </div>

                <button type="submit" class="btn btn-primary">@lang('Reset Password')</button>
            </div>
        </div>
        
    </form>
@endsection
