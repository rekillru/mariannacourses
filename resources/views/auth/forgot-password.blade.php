@extends('layouts/app')

@section('page')
    <h1>@lang('Reset password')</h1>
    <div class="help-block">
        @lang('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.')
    </div>
    @error('*')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <form method="POST" action="{{ route('password.email') }}">
        @csrf

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="">@lang('Email')</label>
                    <input class="form-control" type="email" name="email" required value="{{ old('email') }}" />
                </div>

                <button type="submit" class="btn btn-primary">@lang('Email Password Reset Link')</button>
            </div>
        </div>
        
    </form>
@endsection
