@extends('layouts/app')

@section('page')
<div class="row">
    <div class="col-md-9">
        {{ Breadcrumbs::render('lesson', $lesson->course, $lesson) }}
        <h1>@lang('Lesson'): {{ $lesson->name }}</h1>
        {!! $lesson->content !!}
        <br>
        <br>
        @if (auth()->user()->is_admin)
            @if ($user && $interactive)
                <h3>@lang('Dialog with :name', ['name' => $user->name]):</h3>
                @each('interactive_message', $interactive->messages, 'interactive')
                @include('alert')
                @include('error')
                <form action="{{ route('interactive.store', [$lesson, $user]) }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="">@lang('Your ask'):</label>
                        <textarea required name="content" class="form-control summernotelite">{{ old('content') }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-default">@lang('Send')</button>
                </form>
            @endif
        @else
            <h3>@lang('Dialog with teacher'):</h3>
            @if ($interactive)
                @each('interactive_message', $interactive->messages, 'interactive')
            @endif
            @include('alert')
            @include('error')
            <form action="{{ route('interactive.store', $lesson) }}" method="post" id="dialog_form">
                @csrf
                <div class="form-group">
                    <label for="">@lang('Your question'):</label>
                    <textarea required name="content" class="form-control summernotelite">{{ old('content') }}</textarea>
                </div>
                <button type="submit" class="btn btn-default">@lang('Send')</button>
            </form>
        @endif
    </div>
    <div class="col-md-3">
        @if ($lesson->poster)
            <img src="{{ $lesson->poster }}" alt="" class="img-responsive">
        @endif
        <h3>@lang('Course'): {{ $lesson->course->name }}</h3>
        @if ($lessons->isNotEmpty())
            <br>
            <h4>@lang('Other lessons'):</h4>
            <ul class="list-unstyled">
                @foreach ($lessons as $l)
                    <li>
                        {{ $l->number }}. <a href="{{ route('lesson.show', $l) }}">{{ $l->name }}</a>
                    </li>
                @endforeach
            </ul>
        @endif
        @auth
            @if (auth()->user()->is_admin)
                <br>
                <div class="text-center">
                    <p class="help-block">@lang('The block below is visible only to the administrator')</p>
                    <a href="{{ route('admin.lesson.edit', $lesson) }}" class="btn btn-default">
                        @lang('Edit lesson')
                    </a>
                </div>
            @endif
        @endauth
    </div>
</div>
    
@endsection
