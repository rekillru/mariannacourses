@extends('layouts/app')

@section('page')
    <div class="row">
        <div class="col-md-9">
            @foreach ($courses as $course)
                <div class="row @if (!$loop->last) border-bottom @endif pt-10 pb-10">
                    <div class="col-sm-3 col-md-2">
                        @if ($course->poster)
                            <a href="{{ route('course.show', $course) }}">
                                <img src="{{ $course->poster }}" alt="" class="img-responsive">
                            </a>
                        @endif
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4>
                            {{ $course->number }}. <a href="{{ route('course.show', $course) }}">
                                {{ $course->name }}
                            </a>
                        </h4>
                        <p>@lang('Lessons'): {{ $course->lessons_count }}</p>
                    </div>
                    <div class="col-sm-3 col-md-2 text-center">
                        <div class="h3">
                            {{ $course->price }} @lang(config('app.currency.' . app()->getLocale()))
                        </div>
                        <p>
                            <a href="{{ route('course.show', $course) }}" class="btn btn-default">
                                @lang('More')
                            </a>
                        </p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
