$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function confirmDelete(url) {
    event.preventDefault();
    Swal.fire({
        title: localization.are_you_sure,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: localization.yes,
        cancelButtonText: localization.no
    }).then((result) => {
        if (result.isConfirmed) {
            jQuery.post(url, {"_method":"DELETE"}, () => {
                location.reload();
            });
        }
    });
};

jQuery(()=>{
    jQuery('.summernote').summernote({
        height: 250,
        maximumImageFileSize: 500*1024, // 500 KB
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'imageUploader', 'video', 'hr']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ]
        // callbacks:{
        //     onImageUpload: (files, editor, welEditable) => {
        //         var form_data = new FormData();
        //         form_data.append('image', files[0]);
        //         jQuery.ajax({
        //             data: form_data,
        //             type: "POST",
        //             url: 'admin/image-uploader',
        //             cache: false,
        //             contentType: false,
        //             processData: false,
        //             success: function(url) {
        //                 $(el).summernote('editor.insertImage', url);
        //             }
        //         });
        //     },
        //     onImageUploadError: (msg) => {
        //         Swal.fire({
        //             icon: 'error',
        //             title: 'Oops...',
        //             text: localization.image_size_too_bigger
        //         });
        //     }
        // }
    });
    jQuery('.summernotelite').summernote({
        height: 150,
        toolbar: [
            ['insert', ['picture', 'video']],
            ['fullscreen']
        ],
        popover: {
            image: [
                ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']]
            ]
        },
        maximumImageFileSize: 500*1024, // 500 KB
        callbacks:{
            onImageUploadError: function(msg){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: localization.image_size_too_bigger
                });
            }
        }
    });
});
