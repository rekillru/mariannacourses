(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(require('jquery'));
    } else {
        factory(window.jQuery);
    }
}(function ($) {
    $.extend($.summernote.plugins, {
        'imageUploader': function(context) {
            var imageLimitation = '';
            if (context.options.maximumImageFileSize) {
                var unit = Math.floor(Math.log(context.options.maximumImageFileSize) / Math.log(1024));
                var readableSize = (context.options.maximumImageFileSize / Math.pow(1024, unit)).toFixed(2) * 1 + ' ' + ' KMGTP'[unit] + 'B';
                imageLimitation = "<small>".concat(context.options.langInfo.image.maximumFileSize + ' : ' + readableSize, "</small>");
            }
            var self = this,
                ui = $.summernote.ui,
                $note = context.layoutInfo.note,
                $editor = context.layoutInfo.editor,
                $editable = context.layoutInfo.editable,
                options = context.options,
                lang = options.langInfo,
                buttonClass = 'btn btn-primary note-btn note-btn-primary note-image-btn',
                body = ['<form class="note-image-form" enctype="multipart/form-data" action="image-uploader"><div class="form-group note-form-group note-group-select-from-files">', '<label for="note-dialog-image-file-' + options.id + '" class="note-form-label">Выберите файл</label>', '<input id="note-dialog-image-file-' + options.id + '" class="note-image-input form-control-file note-form-control note-input" ', ' type="file" name="image" accept="image/*" />', imageLimitation, '</div>', '<div class="form-group note-group-image-url">', '</div>', '<div class="form-group"><input type="hidden" name="set_watermark" value="0"><label for="set_watermark"><input type="checkbox" checked name="set_watermark" id="set_watermark" value="1"> Поставить ватермарк</label></div>', '</form>'].join(''),
                footer = '<input type="button" href="#" class="' + buttonClass + '" value="Вставить">',
                $container = options.dialogsInBody ? $(document.body) : $editor;
            context.memo('button.imageUploader', function () {
                var button = ui.button({
                    contents: ui.icon(options.icons.picture),
                    tooltip: lang.image.image,
                    codeviewKeepButton: true,
                    click:function (e) {
                        context.invoke('imageUploader.show');
                    }
                });
                return button.render();
            });
            this.$dialog = ui.dialog({
                title: 'Вставить изображение',
                body: body,
                footer: footer
            }).render().appendTo($container);
            this.destroy = function () {
                ui.hideDialog(this.$dialog);
                this.$dialog.remove();
            };
            this.show = function () {
                var editorInfo = {};
                this.showImageUploaderDialog(editorInfo).then(function (editorInfo) {
                    ui.hideDialog(self.$dialog);
                    $note.val(context.invoke('code'));
                    $note.change();
                });
            };
            this.showImageUploaderDialog = function(editorInfo) {
                return $.Deferred(function (deferred) {
                    var $form = self.$dialog.find('.note-image-form');
                    var $imageBtn = self.$dialog.find('.note-image-btn');
                    ui.onDialogShown(self.$dialog, function () {
                        $imageBtn.click(function (event) {
                            event.preventDefault();
                            var form_data = new FormData($form[0]);
                            jQuery.ajax({
                                data: form_data,
                                type: "POST",
                                url: 'admin/image-uploader',
                                cache: false,
                                contentType: false,
                                processData: false,
                                success: function(url) {
                                    context.invoke('editor.restoreRange');
                                    context.invoke('editor.insertImage', url);
                                    context.invoke('imageUploader.destroy');
                                }
                            });
                        });
                    });
                    ui.onDialogHidden(self.$dialog, function () {
                        $imageBtn.off();
                        if (deferred.state() === 'pending') {
                            deferred.reject();
                        }
                    });
                    ui.showDialog(self.$dialog);
                });
            };
        }
    });
}));
